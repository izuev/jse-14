package ru.tsc.izuev.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static Status toStatus(final String displayName) {
        if (displayName == null || displayName.isEmpty()) return null;
        for (final Status status : values()) {
            if (status.getDisplayName().equals(displayName)) return status;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
